# Components

GitLab CI works on this project so check the Gitlab [Page]().

# Tutorial

Install node packages.
```
$ npm i
```

Open the index on a browser
```
$ cd public/
$ live-server & webpack
```

# Partner専用コンポーネント

If the user is logged in and looking at a specific entry page, it should be possible to have functionality specific for partner users and that entry.

* Simplest case where user is partner and looking at an entry:  The user could submit an ID for that entry(accession#).  Using the URL template for the partner site, the ResourceEntry class for that partner could be inserted.  New ID registration.

* Once one ID is registered, the user may wish to add another.  Thus New ID registration will still be possible even after one already exists.

* The same partner user may wish to edit the entry.  In this case, the ResourceEntry will be deleted for the partner+accession#+id case, and a new ResourceEntry class will be inserted.

Current Entry page:

```plantuml
Component -> SparqList : retrieve data
SparqList -> Component
Component -> User : Display through component
```

component and api interaction:

```plantuml
Component -> API : check if user is partner
API -> Component
Component -> User : display specific HTML if partner
User -> Component : input data
Component -> API : input data
API -> batch.sparqlist : insert data
API -> Component : result
Component -> User : display result

```

現時点（2020/09）のAPIを使える：
 * partner id を削除

現在のpartner id登録は、WURCS入力＋IDとなっているため、必ず登録フローに入ってしまう。
Entryページの場合は、Accession#+IDのみ送信するため、バリデーションされている前提で直接パートナーのグラフに登録できる。

[APIの仕様](https://gitlab.com/glycosmos/glytoucan/gtc-api/-/blob/master/README.md)
